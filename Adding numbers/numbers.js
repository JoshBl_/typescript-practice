//function with two parameters
//type annotation for parameters
function addNumbers(a, b) {
    //return the sum of adding a and b together
    return a + b;
}
//type annotations
//used to enforce checking!
//although this is not mandatory - good way for writing code for easier readability and maintenance
var age = 26; //number variable
var user = "Josh"; //string variable
var isUpdated = true; //boolean variable
//variables can be declared using var, let and const - same as JavaScript
//can declare an object with inline annotations for each of the properties of the object
var details;
//giving values to the object
details = {
    phoneNum: 849683,
    email: "joshblewitt@gmail.com"
};
//calling the add numbers function with 10 and 15 provided as the arguments for a and b
console.log('Sum of two numbers is: ' + addNumbers(10, 15));
//calling variables
console.log('Name: ' + user);
console.log('Age: ' + age);
console.log('Is a developer? ' + isUpdated);
console.log(details);
