//function with two parameters
//type annotation for parameters
function addNumbers (a: number, b:number) {
    //return the sum of adding a and b together
    return a + b
}

//type annotations
//used to enforce checking!
//although this is not mandatory - good way for writing code for easier readability and maintenance
let age: number = 26; //number variable
let user: string = "Josh"; //string variable
let isUpdated: boolean = true; //boolean variable
//variables can be declared using var, let and const - same as JavaScript
//remember - if using let - the scope of the variable is limited to the block scope!
//however, let variables are useful in the sense that they cannot be read or written before they are declared.

//can declare an object with inline annotations for each of the properties of the object
var details : {
    phoneNum: number;
    email: string;
}

//giving values to the object
details = {
    phoneNum: 849683,
    email: "joshblewitt@gmail.com"
}

//calling the add numbers function with 10 and 15 provided as the arguments for a and b
console.log ('Sum of two numbers is: ' + addNumbers(10, 15))

//calling variables
console.log('Name: ' + user);
console.log('Age: ' + age);
console.log('Is a developer? ' + isUpdated);
console.log(details);