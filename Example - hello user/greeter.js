//new class 'student' defined
var Student = /** @class */ (function () {
    //constructor for class - takes arguments for three public fields
    function Student(firstName, middleInitial, lastName, userAge) {
        this.firstName = firstName;
        this.middleInitial = middleInitial;
        this.lastName = lastName;
        this.userAge = userAge;
        //constructor implementation (looking at the instance of the fullName property (string))
        this.fullName = firstName + " " + middleInitial + " " + lastName;
        this.age = userAge;
    }
    return Student;
}());
//the function greeter has an annotation for the argument person - as Person (the interface)
function greeter(person) {
    //will return the firstName and lastName property of person
    return "Hello, " + person.firstName + " " + person.lastName + ". You are " + person.userAge + " years old!";
}
//new let variable called user - defining firstName and lastName
//creating a new instance of the class student and assigning it to the user variable
var user = new Student("Joshua", "R.", "Blewitt", 26);
//calling the function greeter and passing the user variable as an argument
//body and textContent are properties of document (which is a variable)
document.body.textContent = greeter(user);
