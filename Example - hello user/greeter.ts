//new class 'student' defined
class Student {
    //object defined
    fullName: string;
    age: number;
    //constructor for class - takes arguments for three public fields
    constructor(public firstName: string, public middleInitial: string, public lastName: string, public userAge: number) {
        //constructor implementation (looking at the instance of the fullName property (string))
        this.fullName = firstName + " " + middleInitial + " " + lastName;
        this.age = userAge
    }
}

//defining an interface called person that describes two objects (firstName and lastName) as strings
interface Person {
    firstName: string;
    lastName: string;
    userAge: number;
}

//the function greeter has an annotation for the argument person - as Person (the interface)
function greeter (person: Person) {
    //will return the firstName and lastName property of person
    return "Hello, " + person.firstName + " " + person.lastName + ". You are " + person.userAge + " years old!"
}

//new let variable called user - defining firstName and lastName
//creating a new instance of the class student and assigning it to the user variable
let user = new Student ("Joshua", "R.", "Blewitt", 26);

//calling the function greeter and passing the user variable as an argument
//body and textContent are properties of document (which is a variable)
document.body.textContent = greeter(user)