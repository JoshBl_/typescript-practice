//performs a get request on an API and prints the results to a HTML file
function call (theURL:string)
{
    var Http = new XMLHttpRequest();
    Http.open("GET", theURL, false);
    Http.send(null);
    return Http.responseText;
}

//display the results of performing the get request on a web page
document.body.textContent = call("https://api.exchangeratesapi.io/latest?base=")