//new variable defined
var num = 1;
//for every number in num variable, while it's less than 100, increment it by 1
for (num; num < 100; num++) {
    //no type annotation for this variable, it could be a string or a number!
    var output = "";
    if (num % 3 == 0)
        output += "Fizz!";
    if (num % 5 == 0)
        output += "Buzz!";
    //log the output to the console
    console.log(output || num);
}
