//defining a new class
class VideoGame {
    //constructor with three properties and annotations
    constructor (public name: string, public platform: string, public year: number) {
        this.name = name;
        this.platform = platform;
        this.year = year;
    }

    //method to display games (calls each property)
    displayGames() {
        console.log('Name : ' + this.name)
        console.log('Platform : ' + this.platform)
        console.log('Year : ' + this.year + '\n')
    }
}

//sub class that inherits from VideoGame class
class VideoConsole extends VideoGame {
    constructor(public name: string, public year: number, public manufacturer: string, public controllerPorts: number)
    {
        //super declared as this is a sub class - all parameters from the parent class need to be defined in super, even if you give them a default value!
        super (name, 'Console', year);
        this.manufacturer = manufacturer;
        this.controllerPorts = controllerPorts;
    }

    //method to display consoles (calls each property)
    displayConsoles() {
        console.log('Name : ' + this.name);
        console.log('Year : ' + this.year);
        console.log('Manufacturer : ' + this.manufacturer);
        console.log('Number of controller ports : ' + this.controllerPorts + '\n');
    }
}

//function with a rest parameter - when you don't know how many parameters you need
function Greet (greeting: string, ...names:string[]) {
    console.log(greeting + " " + names.join(", ") + "!");
}

Greet("Hello", "Joshua", "Blewitt");

console.log("Welcome to your Video Game Database! \nLet's view some games!\n")
//create new instances of the VideoGame class
const WaveRace64 = new VideoGame ('Wave Race 64', "Nintendo 64", 1996)
const tacticsOgre = new VideoGame ('Tactics Ogre: Let us cling together', 'Super Nintendo Entertainment System', 1995)
const elevatorActionReturns = new VideoGame ('Elevator Action Returns', 'Sega Saturn', 1997)

//each instance calls the method displayGames
WaveRace64.displayGames()
tacticsOgre.displayGames()
elevatorActionReturns.displayGames()

console.log("Let's view some consoles!\n")
//create new instances of the VideoConsole sub class
const superNintendo = new VideoConsole ('Super Nintendo Entertainment System', 1992, 'Nintendo', 2)
const segaSaturn = new VideoConsole ('Sega Saturn', 1994, 'Sega', 2)
const nintendo64 = new VideoConsole ('Nintendo 64', 1996, 'Nintendo', 4)

//each instance calls the method displayConsoles
superNintendo.displayConsoles()
segaSaturn.displayConsoles()
nintendo64.displayConsoles()


let newArray = ['Wave Race 64', 'Tactics Ogre: Let us cling together', 'Elevator Action Returns']
console.log('Iterating through an array of game names!')
//for of loop to iterate each element in the array
for (var val of newArray) {
    console.log(val)
}