//a function is  aregular binding where the value of the binding is a function
//for example, we have defined square to refer to a function which will produce the square of a given number!
const square = function (x:number) {
    return x * x;
}

console.log(square(12));