//this function takes no parameters
const makeNoise = function() {
    console.log("Pling!");
}

//this will print "Pling!" to the console
makeNoise();

//this function takes two parameters
//type annotations to ensure only numbers are allowed
const power = function(base:number, exponent:number) {
    let result:number = 1;
    for (let count:number = 0; count < exponent; count++) {
        result *= base;
    }
    //this will return 1024
    //the return statement determines the value the function returns
    //jumps out of the function and gives the returned value to the code that called the function
    return result;
};

//parameters to a function behave like regular bindings, but their initial values are given by the caller of the function, not the function itself
console.log(power(2,10));
//a return keyword with an expression after it will cause the function to return undefined!