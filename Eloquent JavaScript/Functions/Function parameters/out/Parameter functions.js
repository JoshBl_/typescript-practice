//this function takes no parameters
var makeNoise = function () {
    console.log("Pling!");
};
//this will print "Pling!" to the console
makeNoise();
//this function takes two parameters
//type annotations to ensure only numbers are allowed
var power = function (base, exponent) {
    var result = 1;
    for (var count = 0; count < exponent; count++) {
        result *= base;
    }
    //this will return 1024
    //the return statement determines the value the function returns
    //jumps out of the function and gives the returned value to the code that called the function
    return result;
};
console.log(power(2, 10));
//a return keyword with an expression after it will cause the function to return undefined!
