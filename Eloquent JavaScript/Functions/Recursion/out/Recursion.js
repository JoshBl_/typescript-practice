//a function that calls itself is called recursive
//allows some functions to be written in a different style
//for example
function power(base, exponent) {
    if (exponent == 0) {
        return 1;
    }
    else {
        return base * power(base, exponent - 1);
    }
}
console.log(power(2, 3));
//this will return 8
//however, a simple loop is generally cheaper than calling a function multiple times
//always start by writing something that's correct and easy to understand
//but sometimes, recursion is needed to solve problems.
//below is a puzzle - by starting from 1 and repeatedly either adding 5 or multiplying by 3, an infinite set of numbers can produced
//here's a function that given a number 
function findSolution(target) {
    //this inner function does the actual recursing - takes two arguments, the current number and a string of how we reached that number
    function find(current, history) {
        if (current == target) {
            //if the current number is the target number - return the history string
            return history;
        }
        else if (current > target) {
            //if no solution can be found - return null
            return null;
        }
        else {
            return find(current + 5, "(" + history + " + 5)") || find(current * 3, "(" + history + " * 3)");
        }
    }
    return find(1, "1");
}
console.log(findSolution(24));
//this will return (((1 * 3) + 5) * 3)
