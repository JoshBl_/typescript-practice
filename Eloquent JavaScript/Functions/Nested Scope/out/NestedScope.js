//new function with 1 parameter
var hummus = function (factor) {
    //nested function with it's own scope
    //function has 3 parameters
    var ingredient = function (amount, unit, name) {
        //new variable which the value amount times the factor
        var ingredientAmount = amount * factor;
        //if the ingredientAmount is greater than 1, concatenate the string 's' onto the end of each unit parameter
        if (ingredientAmount > 1) {
            unit += "s";
        }
        //print the 
        console.log(ingredientAmount + " - " + unit + " - " + name);
    };
    ingredient(1, "can", "chickpeas");
    ingredient(0.25, "cup", "tahini");
    ingredient(0.25, "cup", "lemon juice");
    ingredient(1, "clove", "garlic");
    ingredient(2, "tablespoon", "olive oil");
    ingredient(0.5, "teaspoon", "cumin");
};
//call the hummus function with a factor of 5
hummus(5);
