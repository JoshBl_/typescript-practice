//new function with 1 parameter
const hummus = function(factor:number) {
    //nested function with it's own scope
    //function has 3 parameters
    const ingredient = function(amount:number, unit:string, name:string) {
        //new variable which the value amount times the factor
        let ingredientAmount:number = amount * factor;
        //if the ingredientAmount is greater than 1, concatenate the string 's' onto the end of each unit parameter
        if (ingredientAmount > 1) {
            unit += "s"
        }
        //print the amount, the unit and name to the console
        console.log(`${ingredientAmount} - ${unit} - ${name}`)
    };
    //call the ingredient function with 3 parameters (amount, unit and name)
    //can't call ingredient function outside of the hummus function scope!
    ingredient(1, "can", "chickpeas");
    ingredient(0.25, "cup", "tahini");
    ingredient(0.25, "cup", "lemon juice");
    ingredient(1, "clove", "garlic");
    ingredient(2, "tablespoon", "olive oil");
    ingredient(0.5, "teaspoon", "cumin")
};

//call the hummus function with a factor of 5
hummus(5);