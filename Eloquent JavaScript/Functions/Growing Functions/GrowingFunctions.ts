function printFarmInventory(cows:number, chickens:number) {
    //take the cows parameter and assign the contents to a string
    let cowString:string = String(cows);
    //counts the length of the string - while it's less than 3 - concatenate with '0'
    while (cowString.length < 3) {
        //concatenate the string
        cowString = "0" + cowString;
    }
    //print the number of cows
    console.log(`${cowString} Cows`);
    //take the chicken parameter and assign the contents to a string
    let chickenString:string = String(chickens);
    //counts the length of the string - while it's less than 3 - concatenate with '0'
    while(chickenString.length < 3) {
        chickenString = "0" + chickenString;
    }
    //print the number of chickens
    console.log(`${chickenString} Chickens`)
}

//call functions and pass parameters
printFarmInventory(7, 11);


//how about a cleaner way?
function printZeroPaddedWithLabel(number:number, label:string) {
    //take the number parameter and assign the contents to a string
    let numberString:string = String(number);
    while (numberString.length < 3) {
        numberString = "0" + numberString;
    }
    console.log(`${numberString} ${label}`)
}

function printFarmInven (cows:number, chickens:number, pigs:number) {
    printZeroPaddedWithLabel(cows, "Cows")
    printZeroPaddedWithLabel(chickens, "Chickens")
    printZeroPaddedWithLabel(pigs, "Pigs")
}

printFarmInven(7, 11, 3)

//better but a little awkward 
//let's try and pick out a single concept - which will be concatenating a zero

function zeroPad (number:number, width:number) {
    //take the number parameter and assign the contents to a string
    let string:string = String(number);
    //while the length of the string varaible is less than the width parameter - do this
    while (string.length < width) {
        string = "0" + string;
    }
    //return string variable once complete
    return string
}
function printFarmInv (cows:number, chickens:number, pigs:number) {
    //calls the zeroPad function and passes in the parameter from the printFarmInv function
    console.log(`${zeroPad(cows, 3)} Cows`);
    console.log(`${zeroPad(chickens, 3)} Chickens`)
    console.log(`${zeroPad(pigs, 3)} Pigs`)
}

printFarmInv(7, 16, 3)

//this makes it easier to read, plus the method can be reused elsewhere