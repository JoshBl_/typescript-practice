 //new function declared, two arguments
 function countChar (input:string, char:string) {
     //new count integer
     let count:number = 0;
     //for loop to count for every character in the input string
     for (let i:number = 0; i < input.length; i++) {
        //if any of the characters from the input variable match with the char variable 
        if (input[i] == char) {
            //increment the count variable by 1
             count++;
         }
     }
     //once it's iterated through the input variabe, return the count integer
     return count;
 }

 function countBs (string) {
     //this calls the countChar function but passes in the string parameter
     console.log(countChar(string, "B"))
 }


countBs("BBC")
 //this will return 2
countBs("AAA")
 //this will return 0

 console.log(countChar("testing", "t"))
 //this will return 2
 console.log(countChar("AAAAaaaa","A"))
 //this will return 4