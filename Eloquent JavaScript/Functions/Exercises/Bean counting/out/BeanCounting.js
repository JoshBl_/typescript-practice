function countChar(input, char) {
    var count = 0;
    for (var i = 0; i < input.length; i++) {
        if (input[i] == char) {
            count++;
        }
    }
    return count;
}
function countBs(string) {
    console.log(countChar(string, "B"));
}
countBs("BBC");
//this will return 2
countBs("AAA");
//this will return 0
console.log(countChar("testing", "t"));
//this will return 2
console.log(countChar("AAAAaaaa", "A"));
//this will return 4
