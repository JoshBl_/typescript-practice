//new function declared
function min (numOne:number, numTwo:number) {
    //if the first argument is greater than the second, return the second argument
    if (numOne > numTwo) {
        return numTwo
    }
    else {
        //else - return the first argument
        return numOne
    }
}

console.log(min(1, 5));
//prints 1
console.log(min(5, 1));
//prints 1
console.log(min(0, 10));
//prints 0
console.log(min(0, -10));
//prints -10