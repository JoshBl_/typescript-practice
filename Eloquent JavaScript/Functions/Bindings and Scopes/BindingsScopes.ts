//every binding has a scope - if it's defined outside of any function or block - it has a global scope
//bindings created for function parameters or inside a function can be only be referenced in that function - known as local
//whenever a function is called, new instances of these bindings are created - each function acts in its own local environment
//let and const are in fact local to the block they are declared in 

let x:number = 10;
if (true) {
    let y:number = 20;
    //var is visible throughout the whole function and global scope
    var z:number = 30;
    console.log(x + y + z)
    //this will print 60
}
//y is not visible here
console.log(x + z)
//this will print 40

//this function is seeing it's 'own' variable called n - not the global one
const halve = function(n:number) {
    return n / 2;
};

let n:number = 10;
console.log(halve(100));
//that will return 50
console.log(n)
//that will return 10