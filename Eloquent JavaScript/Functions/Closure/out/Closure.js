//what happens to local bindings when the function call that created them is no longer active?
//below shows an example - we define a function which will create a local binding
//which returns a functions that will access and return the local binding
function wrapValue(n) {
    var local = n;
    return () = local;
}
var wrap1 = wrapValue(1);
var wrap2 = wrapValue(2);
console.log(wrap1());
//which will return 1
console.log(wrap2());
//which will return 2
