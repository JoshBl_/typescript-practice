console.log("The future says: ", future());
//function declaration - easier to write
//remember function declarations are not part of the regular top-to-bottom flow of control
//they are conceptually moved to the top of their scope and can be used by all the code in that scope
function future() {
    return "You'll have flying cars!";
}
