//the arrow comes after listing the parameter
var power = function (base, exponent) {
    var result = 1;
    for (var count = 0; count < exponent; count++) {
        result *= base;
    }
    return result;
};
console.log(power(1, 5));
//if an arrow function has no parameters at all, its parameter list is just an empty set of paraenthese
var horn = function () {
    console.log("Toot!");
};
horn();
