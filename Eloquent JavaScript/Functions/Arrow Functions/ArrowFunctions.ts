//the arrow comes after listing the parameter
const power = (base:number, exponent:number) => {
    let result:number = 1;
    for (let count:number = 0; count < exponent; count++) {
        result *= base;
    }
    return result;
}

console.log(power(1, 5));

//if an arrow function has no parameters at all, its parameter list is just an empty set of paraenthese
const horn = () => {
    console.log("Toot!");
}

horn();