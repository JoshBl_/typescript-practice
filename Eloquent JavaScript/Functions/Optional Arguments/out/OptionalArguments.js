//functions can be called with different numbers of arguments
//for example, this function tries to imitate the '-' operator by acting on one or two arguments
function minus(a, b) {
    //if parameter b is undefined - it will return a negative value
    if (b === undefined)
        return -a;
    else
        return a - b;
}
console.log(minus(10));
//this will return -10
console.log(10, 5);
//this will return 5
