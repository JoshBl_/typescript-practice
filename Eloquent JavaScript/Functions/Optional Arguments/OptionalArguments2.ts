//if you write an = operator after a parameter in a function followd by an expression, the value of that expression will replace the argument when it is not given
//the power function will make its second argument optional if the value isn't provided or if the value is 'undefined'
function power (base:number, exponent:number = 2) {
    let result:number = 1;
    for (let count:number = 0; count < exponent; count++) {
        result *= base;
    }
    return result;
}

console.log(power(4));
//this will return 16

console.log(power(2, 6));
//this will return 64