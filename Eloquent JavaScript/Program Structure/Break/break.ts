//having a loop produce false is not the only way a loop - we can use break!
for (let current:number = 20;  ; current += 1) {
    if (current % 7 == 0) {
        console.log(current);
        //the break statement will end the loop!
        //if we don't have a break then this will be an infinite loop!
        break;
    }
}
//as 20 doesn't equal to 0 remainder, 21 does - which is the output of this program