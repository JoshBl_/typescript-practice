function greeter(person) {
    return "Hello, " + person.firstName + " " + person.lastName;
}
var user = { firstName: "Josh", lastName: "Blewitt" };
document.body.textContent = greeter(user);
