//a chain of if statements might look better
//this is inherited from C/Java lanauges
switch (prompt("What is the weather like?")) {
    case "rainy":
        console.log("Remember to bring an umbrealla!");
        break;
    case "sunny":
        console.log("Dress lightly");
        break;
    case "cloudy":
        console.log("Go outside.");
        break;
    default:
        console.log("Unknown weather type!");
        break;
}
