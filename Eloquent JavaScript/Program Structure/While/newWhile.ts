let result:number = 1;
let counter = 0;
while (counter < 10) {
    //while the value of counter is less than 10, do the following
    result = result * 2;
    //this is needed to prevent an infinite loop!
    counter++;
}
console.log(result);