//the first part of a for loop initialises the loop, then checks whether the loop continues, then update the loop after every iteration
for (var number = 0; number <= 12; number = number + 2) {
    console.log(number);
}
