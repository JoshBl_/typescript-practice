//same as the while loop but now uses a for loop!
//for loops tend to be shorter and clearer than a while loop
var result = 1;
for (var counter = 0; counter < 10; counter = counter + 1) {
    result = result * 2;
}
console.log(result);
