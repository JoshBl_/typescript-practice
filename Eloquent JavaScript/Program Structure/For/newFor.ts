//same as the while loop but now uses a for loop!
//for loops tend to be shorter and clearer than a while loop
let result:number = 1;
for (let counter:number = 0; counter < 10; counter = counter + 1) {
    result = result * 2;
}
console.log(result);