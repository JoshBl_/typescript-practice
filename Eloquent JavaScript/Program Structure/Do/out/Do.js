//a do loop will always execute the main body of code at least once!
var yourName;
do {
    yourName = prompt("Who are you?");
} while (!yourName);
console.log(yourName);
//this will enforce you to enter a name, it won't accept an empty string!
//The ! operator is used to convert a value to Bolean before negating it, so all strings except "" convert to true
//so the loop continues until you provide a non-empty name!