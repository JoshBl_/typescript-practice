var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
//defining a new class
var VideoGame = /** @class */ (function () {
    //constructor with three properties and annotations
    function VideoGame(name, platform, year) {
        this.name = name;
        this.platform = platform;
        this.year = year;
        this.name = name;
        this.platform = platform;
        this.year = year;
    }
    //method to display games (calls each property)
    VideoGame.prototype.displayGames = function () {
        console.log('Name : ' + this.name);
        console.log('Platform : ' + this.platform);
        console.log('Year : ' + this.year + '\n');
    };
    return VideoGame;
}());
//sub class that inherits from VideoGame class
var VideoConsole = /** @class */ (function (_super) {
    __extends(VideoConsole, _super);
    function VideoConsole(name, year, manufacturer, controllerPorts) {
        var _this = 
        //super declared as this is a sub class - all parameters from the parent class need to be defined in super, even if you give them a default value!
        _super.call(this, name, 'Console', year) || this;
        _this.name = name;
        _this.year = year;
        _this.manufacturer = manufacturer;
        _this.controllerPorts = controllerPorts;
        _this.manufacturer = manufacturer;
        _this.controllerPorts = controllerPorts;
        return _this;
    }
    //method to display consoles (calls each property)
    VideoConsole.prototype.displayConsoles = function () {
        console.log('Name : ' + this.name);
        console.log('Year : ' + this.year);
        console.log('Manufacturer : ' + this.manufacturer);
        console.log('Number of controller ports : ' + this.controllerPorts + '\n');
    };
    return VideoConsole;
}(VideoGame));
//function with a rest parameter - when you don't know how many parameters you need
function Greet(greeting) {
    var names = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        names[_i - 1] = arguments[_i];
    }
    console.log(greeting + " " + names.join(", ") + "!");
}
Greet("Hello", "Joshua", "Blewitt");
console.log("Welcome to your Video Game Database! \nLet's view some games!\n");
//create new instances of the VideoGame class
var WaveRace64 = new VideoGame('Wave Race 64', "Nintendo 64", 1996);
var tacticsOgre = new VideoGame('Tactics Ogre: Let us cling together', 'Super Nintendo Entertainment System', 1995);
var elevatorActionReturns = new VideoGame('Elevator Action Returns', 'Sega Saturn', 1997);
//each instance calls the method displayGames
WaveRace64.displayGames();
tacticsOgre.displayGames();
elevatorActionReturns.displayGames();
console.log("Let's view some consoles!\n");
//create new instances of the VideoConsole sub class
var superNintendo = new VideoConsole('Super Nintendo Entertainment System', 1992, 'Nintendo', 2);
var segaSaturn = new VideoConsole('Sega Saturn', 1994, 'Sega', 2);
var nintendo64 = new VideoConsole('Nintendo 64', 1996, 'Nintendo', 4);
//each instance calls the method displayConsoles
superNintendo.displayConsoles();
segaSaturn.displayConsoles();
nintendo64.displayConsoles();
var newArray = ['Wave Race 64', 'Tactics Ogre: Let us cling together', 'Elevator Action Returns'];
console.log('Iterating through an array of game names!');
//for of loop to iterate each element in the array
for (var _i = 0, newArray_1 = newArray; _i < newArray_1.length; _i++) {
    var val = newArray_1[_i];
    console.log(val);
}
