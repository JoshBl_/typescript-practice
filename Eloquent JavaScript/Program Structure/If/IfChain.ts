let num:number = Number(prompt("Pick a number!"));
//if the number is less than 10 - print the message below
if (num < 10) {
    console.log("That's a small size number!");
    //if the number is less than 100 - print the message below
} else if (num < 100) {
    console.log("That's a medium size number!");
} else {
    //anything else, print the following
    console.log("That's a big number!");
}