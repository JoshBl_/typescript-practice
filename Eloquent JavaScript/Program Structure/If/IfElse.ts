let theNumber:number = Number(prompt("Pick a number"));
//using the logical operator not (!) - which returns true for false and false for true
if (!Number.isNaN(theNumber)) {
    console.log("Your number is the square root of " + theNumber * theNumber)
} else {
    console.log("Hey, why didn't you give me a number? ");
}