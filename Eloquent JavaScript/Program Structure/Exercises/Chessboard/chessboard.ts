//new variables defined - size set and chessboard set to empty string
let size:number = 8;
let chessboard:string = ""

//first for loop, y initialised, which checks to see if it's less than the size and then increments it by 1
for (let y:number = 0; y < size; y++) {
    //second for loop, x initialised, which checks to if it's less than the size and then increments it by 1
    for (let x:number = 0; x < size; x++) {
        //if the combined value of x and y are divisible by 2 (even) - insert a blank space on the chessboard
        if ((x + y) % 2 == 0) {
            chessboard += " ";
        }
        //if not, enter a #
        else {
            chessboard += "#";
        }
    }
    //after the for loop for the x coordiantes are done, insert a new line
    chessboard += "\n";
}

console.log(chessboard);