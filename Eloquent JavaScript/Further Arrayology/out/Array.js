//push and pop - add and remove elements at the end of an array
//unshift and shift - add and remove elements at the start of an array
//new array
var todoList = [];
//function to push element to array - add to the end
function remember(task) {
    todoList.push(task);
}
//function to shift - remove front element
function getTask() {
    return todoList.shift();
}
//function to unshift - add to the front
function rememberUrgently(task) {
    todoList.unshift(task);
}
remember("Buy food");
rememberUrgently("Learn to code!");
//display array
console.log(todoList);
//return first element and remove it
console.log(getTask());
//display the array after shift
console.log(todoList);
//adding shifted value back in
rememberUrgently("Learn to code!");
//to search for a specific value, we need to use the array method indexOf, which searches from the start
//to search from the end, we need to use lastIndexOf
//if it doesn't find it, -1 is returned
//blank array
var newArray = [];
newArray.push(1);
newArray.push(2);
newArray.push(3);
console.log(newArray);
//find in the array
console.log(newArray.indexOf(3));
console.log(newArray.lastIndexOf(2));
