function loop(value, testfunc, updatefunc, bodyfunc) {
    for (let newValue = value; testfunc(newValue); newValue = updatefunc(newValue)) {
        bodyfunc(newValue);
    }
}
loop(
//starting value
3, 
//testing function, makes sure that n is greater than 0
n => n > 0, 
//update function, decreases the value of n by 1
n => n - 1, 
//body function, print the value of n
console.log);
