function every(array, func){
    for(let element in array) {
        if(!func(element)){
            return false;
        }
        else {
            return true;
        }
    }
}

//some function checks if any of the elements in an array pass a test
function every2(array, func) {
    return !array.some(element => !func(element))
}

//this will return true
console.log(every([1, 3, 5], n => n < 10))

//this will return false
console.log(every([2, 4, 16], n => n < 10))

//this will return true
console.log(every([], n => n < 10))
