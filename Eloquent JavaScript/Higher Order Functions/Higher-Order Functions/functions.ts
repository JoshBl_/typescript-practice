//functions that operate on other functions (as arguments or by returning them) are higher-order functions
//these allow us to abstract over actions, not just values

//we can have functions that create new functions
function greaterThan(n){
    return m => m > n;
}
let greaterThan10 = greaterThan(10);
//greaterThan10 is referenced as 'm' in the greaterThan function
console.log(greaterThan10(11));

//we can have functions that can change other functions
function noisy(f) {
    return (...args) => {
        console.log("Calling with; ", args);
        let result = f(...args);
        console.log("called with: ", args, ", returned ", result);
        return result;
    };
}
noisy(Math.min)(3, 2, 1)

//we can write functions that provide new types of control flow
function unless(test, then) {
    if(!test) then();
}

function repeat(n, action) {
    for (let i = 0; i < n; i++) {
      action(i);
    }
  }

repeat(3, n => {
    unless(n % 2 == 1, () => {
        console.log(n, "is even")
    })
})