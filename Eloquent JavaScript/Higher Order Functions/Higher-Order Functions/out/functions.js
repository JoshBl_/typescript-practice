//functions that operate on other functions (as arguments or by returning them) are higher-order functions
//these allow us to abstract over actions, not just values
//we can have functions that create new functions
function greaterThan(n) {
    return function (m) { return m > n; };
}
var greaterThan10 = greaterThan(10);
//greaterThan10 is referenced as 'm' in the greaterThan function
console.log(greaterThan10(11));
//we can have functions that can change other functions
function noisy(f) {
    return function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        console.log("Calling with; ", args);
        var result = f.apply(void 0, args);
        console.log("called with: ", args, ", returned ", result);
        return result;
    };
}
noisy(Math.min)(3, 2, 1);
//we can write functions that provide new types of control flow
function unless(test, then) {
    if (!test)
        then();
}
function repeat(n, action) {
    for (var i = 0; i < n; i++) {
        action(i);
    }
}
repeat(3, function (n) {
    unless(n % 2 == 1, function () {
        console.log(n, "is even");
    });
});
