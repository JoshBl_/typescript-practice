require('./scripts');
//this function helps filter out the elements in an array that don't pass a test
function filter(array, test) {
    //new blank array
    let passed = [];
    for (let element of array) {
        //uses the argument named test - a function vale
        //used to fill a gap in the computation (process of deciding which elements to collect)
        if (test(element)) {
            //if the test passes, push the element to the array
            passed.push(element);
        }
    }
    //return the array after all elements in the array have been looked at
    return passed;
}
//this function is 'pure'
//it doesn't modify the array it is given
//filter is a standard array method
console.log(filter(SCRIPTS, script => script.living));
//TRANSFORMING WITH MAP
//we can use the map method to transform an array by applying a function to all of its element and building a new array for the returned values
//the new array will have the same length as the input array, but its content will have been mapped to a new form by the function
function map(array, transform) {
    //new blank array
    let mapped = [];
    for (let element of array) {
        //pushes each transformed element to the mapped array
        mapped.push(transform(element));
    }
    return mapped;
}
//new variable where the value is based on the scripts.js file where the direction property = 'rtl'
let rtlScripts = SCRIPTS.filter(s => s.direction == "rtl");
//call map function with rtlScripts variable and transforming based on the name property in scripts.js
console.log(map(rtlScripts, s => s.name));
//SUMMARISING WITH REDUCE
//another common thing to do with arrays is to compute a single value from them
//summing a collection of numbers and finding a script with the most characters are examples
//the higher-order operation that represents this is reduce (or fold)
//builds a value by repeatedly taking a single element from the array and combining with the current value.
//for summing numbers, you'd start with 0 and for every element, add to that sum
//apart from the array, the parameters for reduce are a combining function and a start vlaue
function reduce(array, combine, start) {
    //new variable, the value of current is equal to start
    let current = start;
    //for each element in the array...
    for (let element of array) {
        current = combine(current, element);
    }
    return current;
}
//calling the reduce function, passing an array, a function (to add numbers together) and a start point (0)
console.log(reduce([1, 2, 3, 4], (a, b) => a + b, 0));
//to use reduce (twice) to find the script with the most characters...
//the characterCount function reduces the ranges assigned to a script by summing their sizes
//the second call to reduce then uses this to find the largest script by repeatedly comparing two scripts and returning the larger one
function characterCount(script) {
    return script.ranges.reduce((count, [from, to]) => {
        return count + (to - from);
    }, 0);
}
console.log(SCRIPTS.reduce((a, b) => {
    return characterCount(a) < characterCount(b) ? b : a;
}));
//COMPOSABILITY
//this is the same function as above, but without higher-order functions
let biggest = null;
for (let script of SCRIPTS) {
    if (biggest == null ||
        characterCount(biggest) < characterCount(script)) {
        biggest = script;
    }
}
//it's not that different!
console.log(biggest);
//let's look at composing operations
//this finds the average year of origin for living and dead scripts in the data set
function average(array) {
    return array.reduce((a, b) => a + b) / array.length;
}
//this will return 1165
console.log(Math.round(average(SCRIPTS.filter(s => s.living).map(s => s.year))));
//this will return 204
console.log(Math.round(average(SCRIPTS.filter(s => !s.living).map(s => s.year))));
//you could write this as one loop - this will return 1165
//but this is harder to see what was being computed and how
let total = 0, count = 0;
for (let script of SCRIPTS) {
    if (script.living) {
        total += script.year;
        count += 1;
    }
}
console.log(Math.round(total / count));
//STRINGS AND CHARACTER CODES
//One use of the data set would be figuring out what script a piece of text is using
//remember - each script has an array of character code ranges associated with it.
//so given a character code, we could use a function like this to find the corresponding script
function characterScript(code) {
    for (let script of SCRIPTS) {
        //the 'some' method is another high-order function
        //it takes a test function and tells you whether that function returns true for any of the elements in the array
        if (script.ranges.some(([from, to]) => {
            //if any returns true...
            return code >= from && code < to;
        })) {
            //return the script variable
            return script;
        }
    }
    return null;
}
//this will return Latin object
console.log(characterScript(121));
//JS strings are encoded as a sequence of 16 bit numbers - these are code units
//a Unicode character code was initially supposed to fit within such a unit
//however, there wasn't going to be enough, to adddress the issue, UTF-16 was invented
//this is the format used by JS string
//UTF-16 is considered a bad idea today, if your language doesn't use two-unit characters, it will appear to work
//but as soon as someone tries to use a program with less common Chinese characters, it breaks
//With emoji's - everybody has started using two-unit characters, the burden of dealing with such problems is more fairly distributed
//sadly, operations on JavaScript string, such as getting length and accessing the content using square brackets deal with code units
//two emojis
let horseShoe = "🐴👟";
//this returns 4
console.log(horseShoe.length);
//invalid half-character
console.log(horseShoe[0]);
//code of the half-character
console.log(horseShoe.charCodeAt(0));
//actual code for horse emoji
console.log(horseShoe.codePointAt(0));
//charCodeAt method gives the code unit, not the full character code
//codePointAt method does give a full Unicode character
//you can loop over a string, this gives real characters, not code units
//NOTE: the 'tsconfig' JSON file has the compiler target set to ES2016 to compile the JS file
let roseDragon = "🌹🐉";
for (let char of roseDragon) {
    console.log(char);
}
//RECOGNIZING TEXT
//lets create a script that counts the characters that belong to each script
function countBy(items, groupName) {
    let counts = [];
    for (let item of items) {
        let name = groupName(item);
        let known = counts.findIndex(c => c.name == name);
        if (known == -1) {
            counts.push({ name, count: 1 });
        }
        else {
            counts[known].count++;
        }
    }
    return counts;
}
console.log(countBy([1, 2, 3, 4, 5], n => n > 2));
