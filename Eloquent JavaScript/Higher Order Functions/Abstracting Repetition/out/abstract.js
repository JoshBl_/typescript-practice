//common for a program to do something a given number of times - like a for loop
//this will print the value of i
for (var i = 0; i < 10; i++) {
    console.log(i);
}
//we can abstract this as a function
//this calls the console.log 'n' times
function repeatLog(n) {
    for (var i = 0; i < n; i++) {
        console.log(i);
    }
}
//but what about something other than logging numbers?
//'doing something' can be represented as a function and functions are values, we can pass our action as a function value
function repeat(n, action) {
    for (var i = 0; i < n; i++)
        action(i);
}
//this will print 0, 1, 2
repeat(3, console.log);
//we don't have to pass a predefined function to repeat
//it's easier to create a function value on the spot instead
//blank array
var labels = [];
//creating a function value
repeat(5, function (i) {
    labels.push("Unit " + (i + 1));
});
console.log(labels);
//similar to a for loop!
//the body is now written as a function value which is wrapped in the parentheses of the call to repeat
//which is why it has to be closed with the closing brace and closing parentheses
