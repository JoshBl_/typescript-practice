//we can represent a two-by-two table in JavaScript with a four-element array ([1,2,3,4])
//you could use an array containing two-element arrays ([1,2],[3,4])
//or an object with property names - but a flat array is simple and makes expressions that access the table short
//for now we'll interpret the indices to the array as two-bit binary numbers, the leftmost (most significant) digit as the squirrel
//the rightmost (least significant) digit refers to the event variable.
//for example, 10 refers to the case where Jacques did turn into a squirrel, but the event (such as 'pizza') didn't occur
//this happened four times, and since binary 10 is 2 in decimal notation, we'll store this number at index 2 of the array
//this function computes the ϕ coefficient from an array
"require('journal.js')";
function phi(table) {
    return (table[3] * table[0] - table[2] * table[1]) /
        //this is the Math Square Root function
        Math.sqrt((table[2] + table[3]) *
            (table[0] + table[1]) *
            (table[1] + table[3]) *
            (table[0] + table[2]));
}
//this will print 0.068599434
console.log(phi([76, 9, 4, 1]));
//to extract a two-by-two table for a specific event from the journal, we must loop over all entries and tally how many times the event occurs (in relation to transformations)
function tableFor(event, journal) {
    var table = [0, 0, 0, 0];
    for (var i = 0; i < journal.length; i++) {
        var entry = journal[i], index = 0;
        if (entry.events.includes(event))
            index += 1;
        if (entry.squirrel)
            index + 2;
        table[index] += 1;
    }
    return table;
}
console.log(tableFor("pizza", JOURNAL));
