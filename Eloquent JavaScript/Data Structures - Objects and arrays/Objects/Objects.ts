//objects are arbitrary collections of properties
let day1 = {
    //properties are separated by commas
    //each property has a name followed by a colon and a value
    //if a property doesn't exist it will return undefined
    squirrel: false,
    events: ["work", "touched tree", "pizza", "running"]
};
//returns false
console.log(day1.squirrel);
//adding a new property to the object
day1.wolf = false;
//returns false
console.log(day1.wolf)

//properties whose name aren't valid binding names or valid numbers have to be quoted
let descriptions = {
    work: "went to work",
    "touched tree": "Touched a tree"
};

//braces have two meanings in JavaScript - at the start of a statement,, they start a block of statements
//anywhere else, they describe an object

//you can delete properties from an object
let testObject = {left: 1, right: 2};
//this will print 1
console.log(testObject.left);
//this will print 2
console.log(testObject.right);
//this will delete the left property in the object and print undefined
delete testObject.left;
console.log(testObject.left);
//we can check for properties in an object using the 'in' operator
//this will return false - as it doesn't exist
console.log("left" in testObject);
//this will return true - as it exists
console.log("right" in testObject);
//if you want to see what properties an object has, you can use the Object.keys function - this returns an array of strings of the object's property names
console.log(Object.keys(testObject));

//you can copy properties from one object into another
let objectA = {a: 1, b: 2}
Object.assign(objectA, {b: 3, c: 4});
console.log(objectA);

//this is an array of objects
let journal = [
    {events: ["work","touched tree", "pizza", "running", "television"], transform: false},
    {events: ["work","ice cream","cauliflower","lasagna","touched tree","brushed teeth"], transform: false},
    {events: ["weekend","cycling","break","peanuts","beer"], transform: true}
];