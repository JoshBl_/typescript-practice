//set up blank array
var journal = [];
//new function
function addEntry(events, squirrel) {
    //push an object to the array
    //we're just defining a property name - it's shorthand for the same thing
    //if a property name in brace notation isn't followed by a value, it's value is taken from the binding with the same name
    journal.push({ events: events, squirrel: squirrel });
}
addEntry(["work", "touched tree", "pizza", "running", "television"], false);
addEntry(["work", "ice cream", "cauliflower", "lasagna", "touched tree", "brushed teeth"], false);
addEntry(["weekend", "cycling", "break", "peanuts", "beer"], true);
console.log(journal);
