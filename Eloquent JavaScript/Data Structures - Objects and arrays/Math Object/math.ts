//there are various functions for Math - such as .ax, .min and .sqrt
//Math object is used as a container to group a bunch of related functionality
//remember - there is only one Math object, and it's almost never useful as a value
//instead, it provides a namespace so that all these fucntions and values do not have to be global bindings
//having too many global bindings "pollutes" the namespace. The more names taken, the more likely you are to accidentally overwrite the value of existing bindings
//many languages will stop you (or at least warn you) when you are defining a binding with a name that is already taken
//JS does this for bindings declared with let or const, but not standard bindings or bindings declared with var or function

//lets learn about Math.cos, Math.sin, Math.tan (plus the inverse functions; acos, asin and atan)
//there is also Math.PI (yes, it must be used with capital letters)

function randomPointOnCircle(radius:number) {
    //new variable with takes a random number, times it by 2 and then by Pi
    //Math.random will return a random number between zero (inclusive) and one (exclusive) every time it's called
    let angle = Math.random() * 2 * Math.PI;
    //take the radius and multiply it by cos and sin (with the angle) to calculate values for x and y
    return {x: radius * Math.cos(angle),
            y: radius * Math.sin(angle)};
}

console.log(randomPointOnCircle(2));

console.log(Math.random());
console.log(Math.random());
console.log(Math.random());

//if we want a whole random number (instead of a fractional one) - use Math.floor
//this rounds down to the nearest whole number on the result of Math.random

//multiplying the random number by 10 gives a number greater than or qual to 10 and below 10
//Math.floor rounds down os this will produce a number from 0 - 9
console.log("Math.floor: " + Math.floor(Math.random() * 10));

//Math.ceil rounds up to a whole number
console.log("Math.ceil: " + Math.ceil(Math.random()));

//Math.round - to the nearest whole number
console.log("Math.round: " + Math.round(Math.random() * 10));

//Math.abs - takes the absolute value of a number (meaning it negates negative values but leaves positive ones as they are)
console.log("Math.abs: " + Math.abs(Math.random() * 10));