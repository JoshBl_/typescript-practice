//if you want to save data in a file or send it to another computer the network - we need to convert the memory addresses to a description
//we can serialise the data - it's converted into a flat description.
// JSON - JavaScript Object Notation
//all property names have to be surrounded by double quotes and only simple data expressions are allowed - no function calls, bindings etc
//comments are also not allowed
//JS has functions to convert data to and from this format - JSON.stringify and JSON.parse
//converts to JSON
var string = JSON.stringify({ squirrel: false, events: ["weekend"] });
//prints the JSON
console.log(string);
//this will convert the JSON to the value it encodes
//in this example, we're looking at events, which will print ["weeked"]
console.log(JSON.parse(string).events);
