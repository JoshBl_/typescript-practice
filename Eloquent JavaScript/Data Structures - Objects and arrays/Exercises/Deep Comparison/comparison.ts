//new function
function deepEqual(first, second) {
    //compare both values, if they match return true
    //=== finds out if the values should be compared directly
    if (first === second) return true;

    //if either argument is null or not an object return false
    if(first == null || typeof first != "object" ||
    second == null || typeof second != "object") return false;

    //new variables, holds the values of the keys for the first and second object
    let firstKeys = Object.keys(first), secondKeys = Object(second)

    //check if the length of the keys in both objects are not equal in length
    if(firstKeys.length != secondKeys.length) return false

    //for each 
    for(let key of firstKeys) {
        if(!secondKeys.includes(key) || !deepEqual(first[key], second[key])) return false; 
    }
}

//new objects declared
let object = {here: {is: "an"}, object: 2};
let object2 = {here: "nope"};

//this will return true
console.log(deepEqual(object, object))

//this will return false
console.log(deepEqual(object, object2))