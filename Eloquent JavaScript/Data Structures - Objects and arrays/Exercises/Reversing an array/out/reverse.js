var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
function reverseArray(array) {
    var newArray = [];
    for (var i in array) {
        newArray.unshift(array[i]);
    }
    return newArray;
}
function reverseArrayInPlace(array) {
    var reversedArray = [];
    for (var i in array) {
        reversedArray.unshift(array[i]);
    }
    array.splice.apply(array, __spreadArrays([0, array.length], reversedArray));
}
console.log(reverseArray(["A", "B", "C"]));
var arrayValue = [1, 2, 3, 4, 5];
console.log("Before:");
console.log(arrayValue);
reverseArrayInPlace(arrayValue);
console.log("After:");
console.log(arrayValue);
