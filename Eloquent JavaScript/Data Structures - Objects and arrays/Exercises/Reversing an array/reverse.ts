function reverseArray (array) {
    let newArray = [];
    for (let i in array) {
        newArray.unshift(array[i]);
    }
    return newArray;
}

function reverseArrayInPlace (array) {
    let reversedArray = [];
    for (let i in array) {
        reversedArray.unshift(array[i])
    }
    array.splice(0, array.length, ...reversedArray)
}

console.log(reverseArray(["A", "B", "C"]));

let arrayValue = [1, 2, 3, 4, 5]
console.log("Before:");
console.log(arrayValue);
reverseArrayInPlace(arrayValue);
console.log("After:");
console.log(arrayValue);