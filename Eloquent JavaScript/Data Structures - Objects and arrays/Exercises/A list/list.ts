//in JS a list is a nested set of objects
//the first object holding a reference to the second, second to the third and so on
let list = {
    value: 1,
    rest: {
        value: 2,
        rest: {
            value: 3,
            rest: null
        } 
    }
};

//lists can share parts of their structure
//if I create two new values, they are both independent lists, but they are the structure that makes up their last three elements
//the original list is also still a valid three-element list

//function takes array as an argument
function arrayToList (array) {
    //define a new blank list
    let newList = null;
    //new variable 'i' declared, run code whenever it's greater than or equal to 0, once code loops, subtract 1 from 'i'
    for (let i = array.length - 1; i >= 0; i--) {
        //add the value to the new list, the rest property
        newList = {value: array[i], rest: newList}
    }
    return newList;
}

//function takes list as an argument
function listToArray (list) {
    //new, blank array defined
    let newArray = [];
    //new variable node defined, for every value found in the list, push it to the new array
    for (let node = list; node; node = node.rest) {
        newArray.push(node.value)
    }
    //return the new array after the for loop is complete
    return newArray
}

//function takes an element and a list as an argument
function prepend (element, list) {
    //create a blank list
    let newList = null;
    //the new list consists of the element at the front, then the rest of the list
    newList = {value: element, rest: list}
    //return the new list
    return newList;
}

//function takes a list and a number as an argument
function nth (list, number:number) {
    //if the list is empty - return undefined message
    if(!list) return undefined;
    //if the number equals 0 - return the value in the list
    else if (number == 0) return list.value
    //recursive - call the nth function again but subtract the the number by 1
    else return nth(list.rest, number - 1);
}

console.log(arrayToList([10, 20]));
console.log(listToArray(arrayToList([10, 20, 30])));
console.log(prepend(10, arrayToList([20, 30])));
console.log(nth(arrayToList([10, 20, 30]), 1));