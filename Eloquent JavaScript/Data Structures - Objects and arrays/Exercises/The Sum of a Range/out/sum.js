//function declared
function range(start, end) {
    //blank array created
    var result = [];
    //index variable to refer to the element in the array
    var index = 0;
    //count created for the while statement
    var count = 1;
    //push the initial number to the array
    result.push(start);
    //while loop to push each number to the array
    while (count < end) {
        //index value changes to start + 1
        index = start + 1;
        //index pushed to array
        result.push(index);
        //start and count values incremented by 1
        start++;
        count++;
    }
    return result;
}
//function declared
function sum(array) {
    var total = 0;
    //for every element in the array...
    for (var i in array) {
        //take the value of total and add the element in array to it
        total += array[i];
    }
    //return the total value
    return total;
}
console.log(range(1, 10));
console.log(sum(range(1, 10)));
