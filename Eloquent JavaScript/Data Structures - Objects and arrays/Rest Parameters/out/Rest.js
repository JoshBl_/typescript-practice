var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
//you can accept any number of arguments
//Math.max computes all of the arguments it is given
//to write a function - use three dots before the function's last parameter
function max() {
    var numbers = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        numbers[_i] = arguments[_i];
    }
    var result = -Infinity;
    for (var _a = 0, numbers_1 = numbers; _a < numbers_1.length; _a++) {
        var number = numbers_1[_a];
        if (number > result)
            result = number;
    }
    return result;
}
//this will return 9
console.log(max(4, 1, 9, -2));
//the rest parameter is bound to an array containing all further arguments
//if there are other parameters before it, their values aren't part of that array
//when, as in max, it is the only parameter, it will hold all arguments
//this will sprint 7
var numbers = [5, 1, 7];
console.log(max.apply(void 0, numbers));
//this spreads out the array into the function call, passing its elements as separate arguments
//it is possible to include an array like that along with other arguments as in max(9, ...numbers, 2)
//square bracket array notation similarly allows the triple-dot operator to spread another array into the new array
var words = ["never", "fully"];
//this will return ["will", "never", "fully", "understand"]
console.log(__spreadArrays(["will"], words, ["understand"]));
