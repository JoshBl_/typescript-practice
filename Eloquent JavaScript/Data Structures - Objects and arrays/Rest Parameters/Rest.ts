//you can accept any number of arguments
//Math.max computes all of the arguments it is given
//to write a function - use three dots before the function's last parameter
function max(...numbers) {
    let result = -Infinity;
    for (let number of numbers) {
        if (number > result) result = number;
    }
    return result;
}

//this will return 9
console.log(max (4, 1, 9, -2));

//the rest parameter is bound to an array containing all further arguments
//if there are other parameters before it, their values aren't part of that array
//when, as in max, it is the only parameter, it will hold all arguments

//this will sprint 7
let numbers = [5, 1, 7];
console.log(max(...numbers));

//this spreads out the array into the function call, passing its elements as separate arguments
//it is possible to include an array like that along with other arguments as in max(9, ...numbers, 2)

//square bracket array notation similarly allows the triple-dot operator to spread another array into the new array
let words = ["never", "fully"]
//this will return ["will", "never", "fully", "understand"]
console.log(["will", ...words, "understand"]);
