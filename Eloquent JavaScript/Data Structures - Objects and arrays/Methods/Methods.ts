//properties that contain functions are generally called methods of the value they belong to
//here's an example of a property that holds a function value

let doh:string = "Doh";
console.log(typeof doh.toUpperCase);
//that's a function!
console.log(doh.toUpperCase());
//prints DOH

//properties that contain functions are called methods of the value they blong to - so "toUpperCase is a method of a string", for example
//this looks at two methods that can be used to manipulate arrays

let sequence = [1, 2, 3];
sequence.push(4);
sequence.push(5);
//this will print 1, 2, 3, 4, 5
console.log(sequence)
//this will remove 5 from the array and returns it
console.log(sequence.pop())
//this will print 1,2,3,4
console.log(sequence)

