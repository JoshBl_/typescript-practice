//push and pop - add and remove elements at the end of an array
//unshift and shift - add and remove elements at the start of an array

//new array
let todoList = [];

//function to push element to array - add to the end
function remember(task) {
    todoList.push(task)
}

//function to shift - remove front element
function getTask() {
    return todoList.shift();
}

//function to unshift - add to the front
function rememberUrgently(task) {
    todoList.unshift(task)
}

remember("Buy food");
rememberUrgently("Learn to code!");

//display array
console.log(todoList);

//return first element and remove it
console.log(getTask());

//display the array after shift
console.log(todoList);

//adding shifted value back in
rememberUrgently("Learn to code!");

//to search for a specific value, we need to use the array method indexOf, which searches from the start
//to search from the end, we need to use lastIndexOf
//if it doesn't find it, -1 is returned

//blank array
let newArray = [];
newArray.push(1);
newArray.push(2);
newArray.push(3);
newArray.push(4);
newArray.push(5);
console.log(newArray);
//find in the array
//both indexof and lastIndexOf take an optional second argument of where to start searching
console.log(newArray.indexOf(3));
console.log(newArray.lastIndexOf(2));
console.log(newArray);

//then there is slice - which takes start and end indices and returns an array that has only the elements between them.
//the start index is inclusive, the end index is exclusive
//this returns [3,4]
console.log(newArray.slice(2,4));
//this returns [4,5]
console.log(newArray.slice(3));

console.log(newArray);

//if there is no end index - slice will take all of the elements after the start index
//you can omit the start index top copy the entire array

//the concat method can be used to glue arrays together to create a new array

function remove (array, index) {
    console.log("Method start");
    console.log("Step 1 - " + array.slice(0, index));
    console.log("Step 2 - " + array.slice(index + 1));
    return array.slice(0, index).concat(array.slice(index + 1));
    //if you pass concat an argument that is NOT an array - the value will be added to the new array as if it were a one-element array
}

//this will remove 4 and display [1,2,3,5]
console.log(remove(newArray, 3))

console.log(newArray);
