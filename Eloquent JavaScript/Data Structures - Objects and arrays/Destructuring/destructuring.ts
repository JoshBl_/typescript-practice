//lets revisit the phi function
function phi(table) {
    return (table[3] * table[0] - table[2] * table[1]) /
      Math.sqrt((table[2] + table[3]) *
                (table[0] + table[1]) *
                (table[1] + table[3]) *
                (table[0] + table[2]));
  }

  //this function is awkward - we have a binding pointing to our array
  //it'd be better to have bindings for the element of the array

  function improvedPhi([n00, n01, n10, n11]) {
      return (n11 * n00 - n10 * n01) /
      Math.sqrt((n10 + n11) * (n00 + n01) *
    (n01 + n11) * (n00 + n10));
  }

  //this works with bindings created with let, var or const
  //if you know the value you are binding is an arrya, you can use square brackets to look inside of the value, binding it's contents

  //similar for objects, using braces instead of square brackets 
  let {name} = {name: "Joshua", age: 27};
  //this will print Joshua
  console.log(name);

  //you can't destructure null or undefined - you get an error (as you would if you try to access a property of those values)