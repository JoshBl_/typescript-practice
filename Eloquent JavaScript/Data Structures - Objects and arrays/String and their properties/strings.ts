//we can read properties like length and toUpperCase from string values
//but if you try to add a new property, it doesn't stick!

//let kim = "Kim";
//kim.age = 88;
//console.log(kim.age);
//undefined

//remember - values of type string, number and boolean are NOT objects
//these are immutable and cannot be changed!

console.log("joshua".slice(2, 5));
//prints 'shu'

console.log("joshua".indexOf("u"));
//prints 4

//indexOf can search for a string containing more than one character
//whereas the array method looks for a single element
//this will print 11
console.log("one two three".indexOf("ee"));

//the trim method removes whitespace (spaces, newlines, tabs and similar characters) from the start and end of a string
//this will print 'test'
console.log("   test  \n".trim());

//zeroPad also exists as a method, it is called padStart and takes the desired length and padding character as arguments
//this will print 006
console.log(String(6).padStart(3, "0"));

//you can split a string on every occurrence of another string with split and join it again with join.
let sentence = "Joshua is an aspiring developer"
console.log(sentence);
//this will create an array
let words = sentence.split(" ");
console.log(words);
//this will print "Joshua. is. an. aspiring. developer."
console.log(words.join(". "));

//a string can be repeated with the repeat method, which creates a new string containing multiple copies of the original, glued together
//this will print LALALA
console.log("LA".repeat(3));

let string:string = "abc"
console.log(string.length);
//this will return 3
console.log(string[1]);
//this will return b
