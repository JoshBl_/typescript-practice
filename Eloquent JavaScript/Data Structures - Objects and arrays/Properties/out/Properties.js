//myString.length -> we access the length property!
//almost all JavaScript values have properties (except if it's null or undefined)
var myString = "Josh";
console.log(myString.length);
