//myString.length -> we access the length property!
//almost all JavaScript values have properties (except if it's null or undefined)

let myString:string = "Josh";
console.log(myString.length);