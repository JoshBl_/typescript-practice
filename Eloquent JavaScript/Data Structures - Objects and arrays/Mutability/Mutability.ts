//numbers, string and booleans are all immutable - you can't change the values of those types!
//you can derive new values from them and combine them but the value will always remain the same
//for example - if you had a string contains 'cat', it's not possible to change it to 'rat'
//with objects, there is a difference between having two references to the same object and having two different objects that contains the same properties.

//object1 and object2 bindings grasp the same object - so changing the value of object1 will change object2
//you can say they have the same identity
let object1 = {value: 10};
let object2 = object1;
let object3 = {value: 10};

//this will return true
console.log(object1 == object2);

//this will return false
console.log(object1 == object3);

//this will change the value to 15
object1.value = 15;

//this will print 15
console.log(object2.value)

//this will print 10
console.log(object3.value)

//bindings can also be changeable or constant, but this is separate from the way their values behave
//even though number values don't change - you can use a let binding to keep track of a changing number by changing the value the binding points at
//the const binding to an object can iteslf not be changed and will continue to point at the same object, the contents of that object might change

//this is okay
const score = {visitors: 0, home: 0};
score.visitors = 1;

console.log(score.visitors);
console.log(score.home);

//this isn't allowed - score is a const!
//score = {visitors: 1, home: 1};