//an example of an array - list of values stored between square brackets, separated by commas
var listOfNumbers = [2, 3, 5, 7, 11];
//accessing the third element in the array, print 5
console.log(listOfNumbers[2]);
//accessing the first element in the array, print 2
console.log(listOfNumbers[0]);
//accessing the second element in the array, print 3
console.log(listOfNumbers[2 - 1]);
//the square brackets are the notation for getting elements inside an array
//remember the first index of an array is zero, not one
