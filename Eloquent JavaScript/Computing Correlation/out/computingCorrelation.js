"use strict";
exports.__esModule = true;
//run `npm i @types/node` to enable require function
var JOURNAL = require("./journal");
//we can represent a two-by-two table in JavaScript with a four-element array ([1,2,3,4])
//you could use an array containing two-element arrays ([1,2],[3,4])
//or an object with property names - but a flat array is simple and makes expressions that access the table short
//for now we'll interpret the indices to the array as two-bit binary numbers, the leftmost (most significant) digit as the squirrel
//the rightmost (least significant) digit refers to the event variable.
//for example, 10 refers to the case where Jacques did turn into a squirrel, but the event (such as 'pizza') didn't occur
//this happened four times, and since binary 10 is 2 in decimal notation, we'll store this number at index 2 of the array
//this function computes the ϕ coefficient from an array
function phi(table) {
    return (table[3] * table[0] - table[2] * table[1]) /
        //this is the Math Square Root function
        Math.sqrt((table[2] + table[3]) *
            (table[0] + table[1]) *
            (table[1] + table[3]) *
            (table[0] + table[2]));
}
//this will print 0.068599434
console.log(phi([76, 9, 4, 1]));
//to extract a two-by-two table for a specific event from the journal, we must loop over all entries and tally how many times the event occurs (in relation to transformations)
function tableFor(event, journal) {
    var table = [0, 0, 0, 0];
    for (var i = 0; i < journal.length; i++) {
        //going over arrays one element at a time
        var entry = journal[i], index = 0;
        if (entry.events.includes(event))
            index += 1;
        if (entry.squirrel)
            index += 2;
        table[index] += 1;
    }
    return table;
}
//you could write a loop in another way
//for (let entry of JOURNAL) {
//    console.log(`${entry.events.length} events`)
//}
//with the word 'of' after a variable defintion, it will loop over the elements of the value given after 'of'
console.log(tableFor("pizza", JOURNAL));
//this will print [79, 9, 4, 1]
//we need to compute a correlation for every type of event that occurs in the data set.
//first, we need to find every type of event
function journalEvents(journal) {
    //new array called events
    var events = [];
    //loop over elements of journal.js
    for (var _i = 0, journal_1 = journal; _i < journal_1.length; _i++) {
        var entry = journal_1[_i];
        //loop over elements of the entry variable
        for (var _a = 0, _b = entry.events; _a < _b.length; _a++) {
            var event_1 = _b[_a];
            //if an element isn't in the events array - add it
            //using the logical operator not - !
            if (!events.includes(event_1)) {
                events.push(event_1);
            }
        }
    }
    //return the events array
    //by going over all the events and adding those that aren't already in there to the events array, this function collects every type of event
    return events;
}
console.log(journalEvents(JOURNAL));
//so, using that we can see all the correlations!
for (var _i = 0, _a = journalEvents(JOURNAL); _i < _a.length; _i++) {
    var event_2 = _a[_i];
    //new variable called correlation which is assigned the value of the coefficient from an array
    var correlation = phi(tableFor(event_2, JOURNAL));
    //if any correlation value is greater than 0.1 or less than -0.1, print them
    if (correlation > 0.1 || correlation < -0.1) {
        console.log(event_2 + ":", correlation);
    }
}
//lets focus on a particular event
//interate over every element in journal
for (var _b = 0, JOURNAL_1 = JOURNAL; _b < JOURNAL_1.length; _b++) {
    var entry = JOURNAL_1[_b];
    if (entry.events.includes("peanuts") && !entry.events.includes("brushed teeth")) {
        entry.events.push("peanut teeth");
    }
}
console.log("Peanut teeth: " + phi(tableFor("peanut teeth", JOURNAL)));
