//methods are nothing more than properties that hold function values

//new object called rabbit
let rabbit = {
    //property called speak
    speak: function(line) {
        console.log(`The rabbit says '${line}'`)
    }
};

rabbit.speak("I'm alive");
//The rabbit says 'I'm alive.'

//usually a method needs to do something with the object it was called on
//when a function is called as a method - looked up as a property and immediately called (object.binding) - the binding callled 'this' in its body automatically points at the object it was called on

function speak(line) {
    console.log(`The ${this.type} rabbit says '${line}'`);
}

let whiteRabbit = {type: "white", speak}
let hungryRabbit = {type: "hungry", speak}

whiteRabbit.speak("Oh my ears and whiskers, " + "how late it's getting!")
hungryRabbit.speak("I could use a carrot right now.")

//you can think of 'this' as an extra parameter that is passed in a different way
//if you want to pass it explicitly, you can use a function's 'call' method
//which takes 'this' value as its first argument and treats further arguments as normal parameters

speak.call(hungryRabbit, "Burp!")

//each function has its own 'this' binding, whose value depends on the way it is called, you cannot refer to the 'this' of the wrapping scope in a regular function defined with the function keyword
//arrow functions are different - they do not bind their 'this' but can see the 'this' binding of the scope around them
//you can do something like the following, which references this from inside a local function:

function normalise(){
    console.log(this.coords.map(n =>  n / this.length));
}

normalise.call({coords: [0, 2, 3], length: 5})