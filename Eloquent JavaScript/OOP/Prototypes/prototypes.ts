//example
let empty = {};
console.log(empty.toString);
console.log(empty.toString());

//above is an example of a prototype
//a prototype is another object that is used as a fallback source of properties.
//when an object gets a request for a property that it doesn't have - it's prototype will be searched for the property, then the prototype's property etc
//Object.prototype - the entity behind almost all objects

//this will return true
console.log(Object.getPrototypeOf({}) == Object.prototype);

//this will return null
console.log(Object.getPrototypeOf(Object.prototype))
