//Object-orented programming - a set of techniques that use objects as the central principle of program organization

//the core idea of OOP is to divide programs into smaller piece and make each piece responsible for managing its own state
//this way, knowledge of how a peice of the program works can be kept local to that piece
//someone working on the rest of the program does not have to remember or even be aware of that knowledge
//if local details are changed, only the code directly around it needs to be updated

//different pieces of such a program interact with each other throw interfaces, limited sets of functions or bindings that provide functionality
//properties that are part of the interface are called public
//other which are outside code should not be touching, are called private

//its common to put an underscore at the start of property names to indicate those properties are private
//separating interface from implementation is usually called encapsulation